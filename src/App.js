import NearPage from 'pages/Near';

import './App.css';

import { BrowserRouter, Routes, Route } from 'react-router-dom';
import HomePage from 'pages/Home';
import SolanaPage from 'pages/Solana';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/near" element={<NearPage />} />
          <Route path="/solana" element={<SolanaPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;

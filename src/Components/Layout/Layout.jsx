import React from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './style.css';

function Layout(props) {
  return (
    <div className="layout">
      <div className="layoutHeader">
        <Link to="/" className="mx-2">
          <Button>HOME</Button>
        </Link>
        <Link to="/near" className="mx-2">
          <Button>NEAR</Button>
        </Link>
        <Link to="/solana" className="mx-2">
          <Button>SOLANA</Button>
        </Link>
      </div>
      <div className="layoutContent">{props.children}</div>
    </div>
  );
}

export default Layout;

import Layout from 'Components/Layout/Layout';
import * as nearAPI from 'near-api-js';
import { connect, WalletConnection } from 'near-api-js';
import React, { useEffect, useState } from 'react';
import { Button, InputGroup } from 'react-bootstrap';
import { useLocation } from 'react-router';

const { keyStores, utils } = nearAPI;

const keyStore = new keyStores.BrowserLocalStorageKeyStore();

const config = {
  networkId: 'testnet',
  keyStore,
  nodeUrl: 'https://rpc.testnet.near.org',
  walletUrl: 'https://wallet.testnet.near.org',
  helperUrl: 'https://helper.testnet.near.org',
  explorerUrl: 'https://explorer.testnet.near.org',
};

function NearPage() {
  const location = useLocation();
  console.log('❗TuanHQ🐞 💻 NearPage 💻 location', location);
  // STATE

  const [wallet, setWallet] = useState();

  const [account, setAccount] = useState(null);
  const [balance, setBalance] = useState(null);

  const [address, setAddress] = useState();
  const [amount, setAmount] = useState();

  const [result, setResult] = useState('');

  // END STATE

  // HANDLERS FUNCTIONS
  const handleSignIn = async () => {
    const result = await wallet.requestSignIn();
    return result;
  };

  const handleLogout = () => {
    wallet.signOut();
    setAccount(null);
  };

  const handleGetAccount = async () => {
    const result = await wallet.account();
    setAccount(result);
  };

  const handleChangeAddress = (e) => {
    setAddress(e.target.value);
  };

  const handleChangeAmount = (e) => {
    setAmount(e.target.value);
  };

  const handleSend = async () => {
    if (!amount || !address || amount === '' || address.trim() === '') {
      console.error("Can't send empty amount or address");
      return;
    }

    const rs = await account.sendMoney(
      address,
      utils.format.parseNearAmount(amount.toString())
    );
    setResult(JSON.stringify(rs));
  };

  // END HANDLERS FUNCTIONS

  // LOGIC FUNCTIONS
  const init = async () => {
    const near = await connect(config);
    const wallet = new WalletConnection(near, 'my-app');

    const rs = await wallet.isSignedIn();
    if (rs) {
      const result = await wallet.account();

      const balance = await result.getAccountBalance();

      setAccount(result);
      setBalance(balance);
    }
    setWallet(wallet);
  };

  const getTransaction = () => {
    if (location.search.includes('transactionHashes')) {
      const transaction = location.search.split('?transactionHashes=')[1];
      setResult(transaction);
    }
  };

  // END LOGIC FUNCTIONS

  // EFFECTS
  useEffect(() => {
    init();
  }, []);

  useEffect(() => {
    getTransaction();
  }, [location]);

  // END EFFECTS

  return (
    <Layout>
      {account ? (
        <div className="container">
          <h3>Account ID: {account?.accountId}</h3>
          <h4>
            Balance:{' '}
            {parseFloat(
              utils.format.formatNearAmount(balance?.available)
            ).toFixed(4)}{' '}
            NEAR
          </h4>
          <Button onClick={handleLogout}>Log out</Button>

          <div>
            <h4>Send</h4>
            <span>Address: </span>
            <input
              type="text"
              style={{ width: '400px' }}
              onChange={handleChangeAddress}
              value={address}
            />
            <p></p>
            <span>Amount: </span>
            <input
              type="number"
              style={{ width: '400px' }}
              onChange={handleChangeAmount}
              value={amount}
            />
            <p>(test: hieupt.testnet | 1)</p>
            <Button onClick={handleSend}>Send</Button>
            <p></p>
            View transaction:
            <a
              href={`https://explorer.testnet.near.org/transactions/${result}`}
              target="_blank"
              rel="noreferrer"
            >
              {result}
            </a>
          </div>
        </div>
      ) : (
        <div className="container">
          <Button onClick={handleSignIn} className="me-2">
            Sign In
          </Button>
        </div>
      )}
    </Layout>
  );
}

export default NearPage;

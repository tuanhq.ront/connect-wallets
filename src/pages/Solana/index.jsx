import Layout from 'Components/Layout/Layout';
import React, { useEffect, useState } from 'react';
import {
  clusterApiUrl,
  Connection,
  PublicKey,
  Transaction,
  LAMPORTS_PER_SOL,
  SystemProgram,
} from '@solana/web3.js';
import { Button, Spinner, Table } from 'react-bootstrap';
import moment from 'moment';

function SolanaPage() {
  // STATE
  const [provider, setProvider] = useState(undefined);
  const [walletKey, setWalletKey] = useState(undefined);
  const [balance, setBalance] = useState(null);

  const [loading, setLoading] = useState(false);

  const [addressReceiver, setAddressReceiver] = useState('');
  const [amount, setAmount] = useState('');

  const [detailTransaction, setDetailTransaction] = useState(null);
  const [signature, setSignature] = useState(null);

  // END STATE

  // HANDLERS FUNCTIONS
  const handleSend = async () => {
    if (!amount || !addressReceiver || addressReceiver.trim().length === 0) {
      console.error('Please fill all the fields');
      return;
    }

    setLoading(true);
    // Establishing connection
    const connection = new Connection(clusterApiUrl('testnet'), 'confirmed');

    const recieverWallet = new PublicKey(addressReceiver);

    // // Airdrop some SOL to the sender's wallet, so that it can handle the txn fee
    // const airdropSignature = await connection.requestAirdrop(
    //   provider.publicKey,
    //   LAMPORTS_PER_SOL
    // );

    // // // Confirming that the airdrop went through
    // await connection.confirmTransaction(airdropSignature);
    // console.log('Airdropped');

    const transaction = new Transaction().add(
      SystemProgram.transfer({
        fromPubkey: provider.publicKey,
        toPubkey: recieverWallet,
        lamports: LAMPORTS_PER_SOL * amount, //Investing 1 SOL. Remember 1 Lamport = 10^-9 SOL.
      })
    );
    console.log('❗TuanHQ🐞 💻 handleSend 💻 transaction', transaction);

    // Setting the variables for the transaction
    transaction.feePayer = await provider.publicKey;
    console.log(
      '❗TuanHQ🐞 💻 handleSend 💻 transaction.feePayer',
      transaction.feePayer
    );

    const blockhashObj = await connection.getRecentBlockhash();
    transaction.recentBlockhash = await blockhashObj.blockhash;

    // Transaction constructor initialized successfully
    if (transaction) {
      console.log('Txn created successfully', transaction);
    }

    // Request creator to sign the transaction (allow the transaction)
    const signed = await provider.signTransaction(transaction);

    // The signature is generated
    const signature = await connection.sendRawTransaction(signed.serialize());

    // Confirm whether the transaction went through or not
    await connection.confirmTransaction(signature);

    //Signature chhap diya idhar
    console.log('Signature: ', signature);
    setSignature(signature);
    // if(signature)
    setLoading(false);
  };

  const handleGetDetailTransaction = () => {
    if (!signature || signature.trim().length === 0) {
      return;
    }

    const connection = new Connection(clusterApiUrl('testnet'), 'confirmed');
    //get transaction details
    setLoading(true);
    connection
      .getTransaction(signature)
      .then((info) => {
        console.log('Transaction Info: ', info);
        setDetailTransaction(info);
      })
      .catch((err) => {})
      .finally(() => {
        setLoading(false);
      });
  };
  // END HANDLERS FUNCTIONS

  // LOGIC FUNCTIONS
  const getProvider = () => {
    if (window.solana) {
      const provider = window.solana;

      if (provider.isPhantom && provider) return provider;
    }
  };

  /**
   * @description prompts user to connect wallet if it exists
   */
  const connectWallet = async () => {
    const { solana } = window;

    if (solana) {
      try {
        const response = await solana.connect();
        console.log('wallet account ', response.publicKey.toString());
        setWalletKey(response.publicKey.toString());
      } catch (err) {
        // { code: 4001, message: 'User rejected the request.' }
      }
    }
  };

  /**
   * @description disconnect Phantom wallet
   */
  const disconnectWallet = async () => {
    // @ts-ignore
    const { solana } = window;

    if (walletKey && solana) {
      const a = await solana.disconnect();
      setWalletKey(undefined);
    }
  };

  const getBalance = async () => {
    const connection = new Connection(clusterApiUrl('testnet'), 'confirmed');

    setLoading(true);
    connection
      .getBalance(new PublicKey(walletKey))
      .then((res) => {
        setBalance(res / 10 ** 9);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  // END LOGIC FUNCTIONS

  // EFFECTS
  useEffect(() => {
    const provider = getProvider();

    if (provider) setProvider(provider);
    else setProvider(undefined);
  }, []);

  // END EFFECTS
  return (
    <Layout>
      {!provider && (
        <p className="container">
          No provider found. Install{' '}
          <a href="https://phantom.app/">Phantom Browser extension</a>
        </p>
      )}
      {provider && !walletKey && (
        <Button onClick={connectWallet} className="me-2">
          Connect to Phantom Wallet
        </Button>
      )}
      {provider && walletKey && (
        <div className="container">
          <p>
            <b>Connected account: </b> {walletKey}
          </p>
          <p>
            <b>Balance: </b> {balance ? balance : '__'} SOL
          </p>
          <Button onClick={disconnectWallet} className="me-2">
            Disconnect
          </Button>
          <Button onClick={getBalance}>Get Balance</Button>

          <h4>Send</h4>

          <span>Public key receiver wallet: </span>
          <input
            type="text"
            style={{ width: '500px' }}
            value={addressReceiver}
            onChange={(e) => {
              setAddressReceiver(e.target.value);
            }}
          />
          <p></p>
          <span>Amount: </span>
          <input
            type="number"
            style={{ width: '500px' }}
            value={amount}
            onChange={(e) => {
              setAmount(e.target.value);
            }}
          />

          <br />
          <Button onClick={handleSend}>Send</Button>
        </div>
      )}
      <h5 className="mt-5">DETAILS Transaction</h5>
      <span>Signature: </span>
      <input
        style={{ width: '1000px' }}
        type="text"
        value={signature}
        onChange={(e) => {
          setSignature(e.target.value);
        }}
      />
      <p></p>
      <Button onClick={handleGetDetailTransaction}>
        Get detail Transaction
      </Button>

      {detailTransaction && (
        <Table striped bordered hover size="sm" className="mt-3">
          <tbody>
            <tr>
              <td>Signature</td>
              <td>{detailTransaction.transaction.signatures[0]}</td>
            </tr>
            <tr>
              <td>Result</td>
              <td>Mark</td>
            </tr>
            <tr>
              <td>Timestamp</td>
              <td>
                {moment(detailTransaction.blockTime * 1000).format(
                  'HH:mm:ss DD/MM/YYYY'
                )}
              </td>
            </tr>
            <tr>
              <td>Confirmation Status</td>
              <td> - - </td>
            </tr>
            <tr>
              <td>Confirmations</td>
              <td> - - </td>
            </tr>
            <tr>
              <td>Slot</td>
              <td>{detailTransaction.slot}</td>
            </tr>
            <tr>
              <td>Recent Blockhash</td>
              <td>{detailTransaction.transaction.message.recentBlockhash}</td>
            </tr>
            <tr>
              <td>Fee (SOL)</td>
              <td>{detailTransaction.meta.fee / 10 ** 9}</td>
            </tr>
          </tbody>
        </Table>
      )}

      <hr />
      {loading && (
        <>
          <Spinner animation="grow" variant="primary" />
          <Spinner animation="grow" variant="secondary" />
          <Spinner animation="grow" variant="success" />
          <Spinner animation="grow" variant="danger" />
          <Spinner animation="grow" variant="warning" />
          <Spinner animation="grow" variant="info" />
          <Spinner animation="grow" variant="light" />
          <Spinner animation="grow" variant="dark" />
        </>
      )}
      <p>(Ctrl + F5 if not display button)</p>
    </Layout>
  );
}

export default SolanaPage;
